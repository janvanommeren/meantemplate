﻿module.exports = function (app, controller, authHelper) {
    app.get('/users', authHelper.ensureAdmin, controller.list);
    app.get('/users/:id', authHelper.ensureAdmin, controller.find);
    app.post('/users', authHelper.ensureAdmin, controller.create);
    app.put('/users/:id', authHelper.ensureAdmin, controller.update);
    app.delete('/users/:id', authHelper.ensureAdmin, controller.delete);
}