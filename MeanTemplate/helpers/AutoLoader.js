﻿var mongoose = require('mongoose');
var fs = require('fs');

module.exports = function (app, ensureAuthenticated) {
    
    var controllerPath = __dirname + '/../controllers';
    var routesPath = __dirname + '/../routes';
    var modelsPath = __dirname + '/../models';
    
    function loadModels() {
        var models = fs.readdirSync(modelsPath);
        models.forEach(function (file) {
            require(modelsPath + '/' + file);
        });
    }
    
    function createMongoModel(restName) {
        try {
            var model = mongoose.model(restName);
            return model;
        } catch (exception) {
            console.log('AutoLoader: mongoose model not found or invalid: ' + exception);
            return null;
        }
    }
    
    function loadController(restName, model) {
        var controllerName = restName + 'Controller.js';
        try {
            var controller = require(controllerPath + '/' + controllerName)(mongoose, model);
            return controller;
        } catch (exception) {
            console.log('AutoLoader: controller not found or invalid: ' + exception);
            return null;
        }
    }
    
    function loadRoute(file, app, controller, ensureAuthenticated) {
        try {
            require(routesPath + '/' + file)(app, controller, ensureAuthenticated);
        } catch (exception) {
            console.log('AutoLoader: invalid route: ' + exception);
        }
    }
    
    return {
        autoLoad: function () {
            loadModels();
            var routes = fs.readdirSync(routesPath);
            routes.forEach(function (file) {
                var restName = file.replace('Routes.js', '');
                var model = createMongoModel(restName);
                var controller = loadController(restName, model);
                loadRoute(file, app, controller, ensureAuthenticated);
            });
        }
    };
};