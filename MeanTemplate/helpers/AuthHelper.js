﻿module.exports = function (app, config) {
    
    var passport = require('passport');
    var session = require('express-session');
    var LocalStrategy = require('passport-local').Strategy;
    var mongoose = require('mongoose');
    var userSchema = require(__dirname + '/../models/User.js');
    var User = mongoose.model('User');

    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });
    
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });
    
    passport.use(new LocalStrategy({ usernameField: 'email' }, function (email, password, done) {
        User.findOne({ email: email }, function (err, user) {
            if (err) return done(err);
            if (!user) return done(null, false);
            user.comparePassword(password, function (err, isMatch) {
                if (err) return done(err);
                if (isMatch) return done(null, user);
                return done(null, false);
            });
        });
    }));
    
    app.use(session({
        secret: config.secret,
        saveUninitialized: true,
        resave: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(function (req, res, next) {
        if (req.user) {
            res.cookie('user', JSON.stringify(req.user));
        }
        next();
    });
    
    //set routes
    app.post('/login', passport.authenticate('local'), function (req, res) {
        res.cookie('user', JSON.stringify(req.user));
        res.send(req.user);
    });
    app.post('/signup', function (req, res, next) {
        var user = new User({
            email: req.body.email,
            password: req.body.password
        });
        user.save(function (err) {
            if (err) return next(err);
            res.send(200);
        });
    });
    app.get('/logout', function (req, res, next) {
        req.logout();
        res.send(200);
    });

    return {
        app: app,
        ensureAuthenticated: function (req, res, next) {
            if (req.isAuthenticated()) next();
            else res.send(401);
        },
        ensureAdmin: function (req, res, next) {
            if (!req.user || !req.user._doc._id) { res.send(401); }
            User.findById(req.user._doc._id, function (err, user) {
                if (!err && req.user.password === user.password && req.isAuthenticated && user.isAdmin) {
                    next();
                } else {
                    res.send(401);
                }
            });
        }
    };
}