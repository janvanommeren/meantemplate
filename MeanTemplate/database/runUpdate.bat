@echo off

call config.bat
start /WAIT /B "Mongo Client" /D %mongoDir% mongo %host%:%port%/%database% %mainScript% --eval "cd('%scriptsDir%')"
pause