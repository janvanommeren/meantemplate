db.createCollection('users');
var userCol = db.getCollection('users');

userCol.insert({
    name: 'admin',
    email: 'janvanommeren@hotmail.com',
    password: '$2a$10$dKyrrcuxdlDQVnykiqbRCu.bICG.BHz8EYgoO7ekGaRtA7JYMxMme',
    isAdmin: true
});

userCol.insert({
    name: 'user',
    email: 'janvanommeren@gmail.com',
    password: '$2a$10$dKyrrcuxdlDQVnykiqbRCu.bICG.BHz8EYgoO7ekGaRtA7JYMxMme',
    isAdmin: false
});