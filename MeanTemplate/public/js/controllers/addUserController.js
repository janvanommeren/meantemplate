﻿meanTemplate.controller('AddUserController', ['$scope', 'UserFactory', 'ngDialog',
    function ($scope, UserFactory, ngDialog) {
        //todo: update users on edit and create
        $scope.headText = 'Toevoegen';

        initUser();
        function initUser() {
            if ($scope.ngDialogData && $scope.ngDialogData.user) {
                $scope.headText = 'Bewerken';
                $scope.user = {};
                $scope.user.email = $scope.ngDialogData.user.email;
                $scope.user.name = $scope.ngDialogData.user.name;
                $scope.user.isAdmin = $scope.ngDialogData.user.isAdmin;
                $scope.user._id = $scope.ngDialogData.user._id;
            }
        }

        $scope.save = function (user) {
            if (user._id) {
                UserFactory.update({ id: user._id }, user, function (res) { });
            } else {
                UserFactory.save({}, user, function (res) { });
            }
            ngDialog.close();
        }
    }
]);