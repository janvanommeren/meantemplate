﻿meanTemplate.controller('SignupController', ['$scope', 'AuthService', function ($scope, AuthService) {
    $scope.signup = function () {
        AuthService.signup({
            email: $scope.email,
            password: $scope.password
        });
    };
}]);