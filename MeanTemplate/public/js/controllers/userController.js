﻿meanTemplate.controller('UserController', ['$scope', '$routeParams', 'UserFactory', '$location', 'ngDialog',
    function ($scope, $routeParams, UserFactory, $location, ngDialog) {

        $scope.loadUsers = function () { 
            $scope.users = UserFactory.get({}, function () { }, 
                function (res) {
                    if (res.status === 401) {
                        $location.path('/admin');
                    }
                }
            );
        }
        $scope.loadUsers();

        //$scope.users = UserFactory.get({id:'547efbeb41a8675d2664f0a6'});
        //$scope.search = function () {
        //    $scope.users = UserFactory.get({ id: $routeParams.id }, function () {
        //        console.log($scope.requests);
        //    });
        //}
        
        $scope.showAdd = function (user) {
            ngDialog.open({
                template: 'partials/back/addUser.html',
                controller: 'AddUserController',
                data: { user: user }
            });
        }

        $scope.delete = function (user) {
            UserFactory.delete({ id: user._id }, function () { 
                loadUsers();
            });
        }
    }
]);