﻿meanTemplate.factory('UserFactory', ['$resource', function ($resource) {
        var actions = {
            'get': { method: 'get' },
            'save': { method: 'post' },
            'update': { method: 'put' },
            'query': { method: 'get', isarray: true },
            'delete': { method: 'delete' }
        };
        return $resource('/users/:id', {id: '@id'}, actions);
        //return $resource('/users/:id');
    }
]);