﻿var meanTemplate = angular.module('meanTemplate', 
    ['ngRoute', 'ngResource', 'ngCookies', 'ngAnimate', 'ngMessages', 'mgcrea.ngStrap', 'ngDialog']);

var config = {
    backendUrl: 'http://localhost:8080'
};

meanTemplate.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'partials/front/home.html'
    }).when('/admin', {
        templateUrl: 'partials/back/login.html',
        controller: 'LoginController'
    }).when('/signup', {
        templateUrl: 'partials/back/signup.html',
        controller: 'SignupController'
    }).when('/users', {
        templateUrl: 'partials/back/users.html',
        controller: 'UserController'
    }).otherwise({
        redirectTo: '/'
    });
}]);