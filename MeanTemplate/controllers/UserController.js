﻿module.exports = function (mongoose, user) {
    return {
        list: function (req, res, next) {
            var conditions = {};
            var fields = {};
            var options = {};
            
            user.find(conditions, fields, options)
            .exec(function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'list',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            });
        },
        find: function (req, res) {
            var conditions = { _id: req.params.id };
            var fields = {};
            var options = {};
            
            user.find(conditions, fields, options)
            .exec(function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'find',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc[0],
                    err: err
                };
                return res.send(retObj);
            });
        },
        create: function (req, res) {
            var doc = new user(req.body);
            doc.save(function (err) {
                var retObj = {
                    meta: {
                        action: 'create',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            });
        },
        update: function (req, res) {
            var conditions = { _id: req.params.id };
            var update = {
                name: req.body.name || '',
                email: req.body.email || ''
            };
            if (req.body.password) {
                update.password = req.body.password;
            }
            var options = { multi: false };
            var callback = function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'update',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            };
            user.findByIdAndUpdate(conditions, update, options, callback);
        },
        delete: function (req, res) {
            var conditions = { _id: req.params.id };
            var callback = function (err, doc) {
                var retObj = {
                    meta: {
                        action: 'delete',
                        timestamp: new Date(),
                        filename: __filename
                    },
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            };
            user.remove(conditions, callback);
        }
    };
};