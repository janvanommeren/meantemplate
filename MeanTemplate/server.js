﻿//external dependencies
var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var mongoose = require('mongoose');

//load config
var config = require('./config/config.js');

//init mongoose
mongoose.set('debug', config.debug);
mongoose.connection.on('error', function (err) {
    console.error('MongoDB error: %s', err);
});
mongoose.connect(config.dbHost);

//express
var app = express();
app.use(express.static(__dirname + '/public')); 	    // set the static files location /public/img will be /img for users
app.use(morgan('dev')); 					            // log every request to the console
app.use(bodyParser.urlencoded({ extended: false }))     // parse application/x-www-form-urlencoded
app.use(bodyParser.json())                              // parse application/json
app.use(methodOverride()); 	                            // simulate DELETE and PUT

//auth
var authHelper = require('./helpers/AuthHelper.js')(app, config);
app = authHelper.app; 

//autoload models, routes and controllers
var autoLoader = require('./helpers/AutoLoader.js')(app, authHelper);
autoLoader.autoLoad();

//start server
app.listen(config.webSrvPort);